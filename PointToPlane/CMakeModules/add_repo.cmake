include(ExternalProject)
include(CMakeParseArguments)

set(INSTALL_DIR ${CMAKE_INSTALL_PREFIX})
add_custom_target(deps COMMENT "Building meshify dependencies.")

function(add_repo)
  set(ADD_REPO_OPTIONS)
  set(ADD_REPO_SINGLE_ARGS PROJECT TAG GIT_URL)
  set(ADD_REPO_MULTI_ARGS DEPENDS CMAKE_ARGS PACKAGES)
  cmake_parse_arguments(repo
    "${ADD_REPO_OPTIONS}"
    "${ADD_REPO_SINGLE_ARGS}"
    "${ADD_REPO_MULTI_ARGS}"
    "${ARGN}"
    )

  if(NOT repo_PROJECT OR NOT repo_TAG)
    message(FATAL_ERROR "Both PROJECT and TAG arguments required for add_repo")
  endif()

  string(TOUPPER ${repo_PROJECT} REPO)
  string(TOLOWER ${repo_PROJECT} repo)
  set(ARGS
    -DCMAKE_INSTALL_PREFIX:PATH=${INSTALL_DIR}
    -DCMAKE_PREFIX_PATH:PATH=${INSTALL_DIR}
    -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
    -DBUILD_SHARED_LIBS:BOOL=OFF
    ${repo_CMAKE_ARGS}
    )
  foreach(pkg ${repo_PACKAGES})
    list(APPEND ARGS -DEXPORT_${pkg}:BOOL=OFF)
  endforeach()
  set(TARGET_NAME DEP_${repo_PROJECT})

  set(DEPS)
  foreach(DEP ${repo_DEPENDS})
    list(APPEND DEPS DEP_${DEP})
  endforeach()

  if(repo_GIT_URL)
    set(GIT_URL ${repo_GIT_URL})
  else()
    set(GIT_URL git@gitlab.com:replicalabs/${repo_PROJECT}.git)
  endif()

  ExternalProject_Add(${TARGET_NAME}
    GIT_REPOSITORY ${GIT_URL}
    GIT_TAG ${repo_TAG}
    DEPENDS ${DEPS}
    CMAKE_ARGS ${ARGS}
    INSTALL_DIR ${INSTALL_DIR}
    )

  set_target_properties(${TARGET_NAME} PROPERTIES EXCLUDE_FROM_ALL ON)
  add_dependencies(deps ${TARGET_NAME})
endfunction()
