/*
  Plane fiting to a point cloud
  We consider the problem of fitting a 3D plane to a point cloud.

  Write the function fitPlaneToPointCloud, returning the plane equation
  that best fits the input point cloud image. You can assume you have
  a linear algebra library available. How would you make it robust to outliers
  and return the dominant plane equation instead?

  Answer: Use RANSAC. TODO(bminortx): Program here
*/

#include <Eigen/Eigen>
#include <Eigen/LU>
#include <iostream>
#include <vector>

struct Vector3f {
  float m[3];
};

struct PlaneEquation // Hessian Normal form
{
  Vector3f normal; // normalized
  float d; // distance to origin
};

PlaneEquation fitPlaneToDepthImage (const std::vector<Vector3f>& pointCloud) {
  float epsilon = 0.05;
  float norm = 100;
  int count = 0;

  Eigen::MatrixXf A = Eigen::MatrixXf(pointCloud.size(), 4);
  for (int i = 0; i < pointCloud.size(); i++) {
    A.row(i) << pointCloud[i].m[0], pointCloud[i].m[1], pointCloud[i].m[2], 1;
  }
  Eigen::Vector4f x = A.fullPivLu().kernel();
  float a = x[0];
  float b = x[1];
  float c = x[2];
  float d = x[3];
  float dist = sqrt(a * a + b * b + c * c);
  PlaneEquation plane;
  Vector3f final_normal;
  final_normal.m[0] = a / dist;
  final_normal.m[1] = b / dist;
  final_normal.m[2] = c / dist;
  plane.normal = final_normal;
  plane.d = d / dist;
  return plane;
}

int main() {
  int num_points = 100;
  std::vector<Vector3f> pointCloud;
  pointCloud.reserve(num_points);
  for (int i = 0; i < num_points; i++) {
    Vector3f point;
    point.m[0] = float(rand() % 50);
    point.m[1] = float(rand() % 10);
    point.m[2] = 9 + float(rand() % 1);
    pointCloud.push_back(point);
  }
  PlaneEquation final_plane = fitPlaneToDepthImage(pointCloud);
  std::cout << "Final plane equation: "
            << final_plane.normal.m[0] << ", "
            << final_plane.normal.m[1] << ", "
            << final_plane.normal.m[2] << ", "
            << final_plane.d
            << std::endl;
}
