## Epipolar, Affine, Projective Geometry: ##

Epipolar Geometry, defined: Projective geometry that relates two different cameras 

Affine Geometry, defined: Geometry in which properties are preserved by parallel projection from one plane to another.

Orthographic Projection: A form of parallel projection, where all the projection lines are orthogonal to the projection plane. Represented by the matrix
|1 0 0 0|
|0 1 0 0|
|0 0 0 0|
|0 0 0 1|
in homogeneous coordinates

Camera Intrinsics: The focal lengths, skew, and euclidian center of a camera. Written as the matrix
K = fx,  s, cx
    0 , fy, cy
    0 ,  0,  1
Is a 3x3 matrix.

Camera Extrinsics: The rotation and translation between the world and the camera (t_wc). Written
T = [R | t]. 4x4 matrix. This means that homogeneous coordinates must be used when going from image to space coordinates.

Epipole: The intersection of the baseline with the image plane. If the epipole is outside of the image, it means that the cameras can't see each other.

Baseline: The line connecting two camera centers in metric space. 

Epipolar Plane: Any plane intersecting the baseline. The possible range of planes is called the pencil of planes. 

Epipolar Line: A line that intersects the epipole and the image plane. Used to relate matching points in two different image planes through the relation:
l' = [e'] x [x'] = [e'] x [Hx]
where H is the homography matrix
also,
l' = F [e] x l and l = F^T [e'] x l'

Infinity:
- Affine: The point at which the homogeneous coordinate z = 0. Defined as the line at infinity.
- Metric: Lines never meet (parallel)

Homography Matrix H:
- Purpose: Relate the image planes of two cameras. 
- Compute from image correspondences:
  x' = Hx
  -> [x', y', w'] = [h1 * x, h2 * x, h3 * x]
  And since x' and x are on the same line (affine transformation), we know that [x'] x [Hx] = 0.
  Therefore, solve for [x'] x [Hx] = 0 using a linear least-squares solver.

Fundamental Matrix F:
- Purpose: The relation between two projective geometries. The fundamental matrix relates a point in space to its corresponding epipolar line in another camera frame.
- Compute from homography: [l'] = [e'] x [x'] = [e'] x [Hx] = Fx
  - Compute from image correspondences:
  Equation for a pair of points (x, y, 1) and (x, y', 1) is
	pt = x'x(f11) + x'y(f12) + x'(f13) + y'x(f21) + y'y(f22) + y'(f23) + x(f31) + y(f32) + f33 = 0
  Set n > 8 point matches into a least squares problem
  [ pt ] f = 0
  Solve using least-squares.
- Compute from camera matrices P and P':
  General cameras:
  F = [e'] x P'P+, where P+ is the pseudo-inverse of P, and e' = P'C, with PC = 0
  Canonical cameras:
  F = [e'] x M = M^-T x [e], where e' = m and e = M^-1 * m

Essential Matrix E:
- Purpose: Fundamental Matrix for calibrated cameras
- Formulation:
  E = [T]x R
  E = K'^T * F * K
  l' = E * x (same as fundamental matrix)

Projection from 2D to 3D:
- Given K, T:
  X = T * piup(K^-1 * x)
  Where piup is the homogeneous transform up to 3d space

Projection from 3D to 2D:
- Given K, T:
  x = K * pidown(T * X)
  Where pidown is the homogeneous transform from 3D to 2D space

Translation from image x' to image x:
x' = K'^-1 * pidown([R|t] * piup(K * x))
Remember to normalize the homogeneous coordinate x beforehand.

Depth of points in front of camera:
- depth(X; P) = [sign(det(K))w] / T||m^3||
  where
  w = P^3T(X-C) = m^3T(Xhat - Chat)
  m^3 = principal ray direction, which is the ray from the center of the camera to the principal point of the image plane (usually the center)
  T = homogeneous coordinate in 3D space, e.g. (X, Y, Z, T)

Depth from pair of stereo cameras:
- Related to the disparity between the pixels in images:
  x - x' = (B * f) / Z,
  where
  B = the distance between the cameras (the baseline length)
  f = the focal length of the camera
  Z = the depth of the 3D point (in metric coordinates)
  Epipolar line search can make this process faster by constraining the problem considerably
  Once we have the depth, we can incorporate it into our point estimation:
  X = Z * [R|t] * piup(K^-1 * x),
  As Z is just a metric scaling of the projection

3D points from depth information:
- If we know depths z, we can build a data matrix C = zp by estimating the depth-scaled image points:
  zp = MX
  where
  M = K * [R|t]
  C is now 3J x N, J being the number of images and N being the number of points in the image.
  Thus, at every image j and scan point Pn:
  z(j,n) * p(j,n) = Mj * Pn
  Thus, stack up the camera matrices M to for a 3J * 4 matrix M, and stack the P terms to create a 4 * N matrix. Thus, we can now obtain a factorization of the data matrix:
  C = MP
  Thus, solve for MP.

## Image Transformations ##

Camera distortion:
- Given k1, k2, and r:
  xd = xu * (1 + k1 * r^2 + k2 * r^4)
  where
  r = sqrt((xu - xc)^2 + (yu - yc)^2)
  (xc, yc) = princpal point of the camera
- Barrel distortion: has a -K1
- Pincushion distortion: has a +K1
- Moustache distortion: has a dominant K2

Taking the Partial Derivative of an Image:
- Forward Difference:
  du/dx(x,y) = [u(i+1, j) - u(i,j)] / h
  where h is the stride of the step
- Sobel Derivative:
  du/dx = [-1, -2, -1]
          [ 0,  0,  0] * G, where G is the value of the pixel
		  [	1,  2,  1]
  du/dy is computed in a similar fashion for both 

Bilinear interpolation:
- Through Linear Least Squares, solve for the following system:
  f(x, y) = a0 + a1 * x + a2 * y + a3 * x * y
  | 1, x1, y1, x1y1| * |a0| = |f(x1, y1)|
  | 1, x1, y2, x1y2|   |a1|   |f(x1, y2)|
  | 1, x2, y1, x2y1|   |a2|   |f(x2, y1)|
  | 1, x2, y2, x2y2|   |a3|   |f(x2, y2)|
  Also possible (preferrable) to calculate analytically.

Laplacian Pyramid:
- Lowpass pyramid: Smooth the image, and then subsample (usually by a factor of 2x). This results in a image that is a quarter of the size of the original

## Optimization Techniques ##

Newton Step:
delta(x) = -(hessian(x))^-1 * jacobian(x)

Gauss-Newton:
delta(x) = -(J^T * J)^-1 * J^T * resid(x)

Levenberg-Marquadt:
Add a line search weight to the Gauss-Newton step

Solutions to A*x = 0:
- LU decomposition with pivoting: involves using digenvectors to find a fast solution to the kernel of A
- Kernel spaces: The mapping from a matrix A to its nullspace

Getting rid of outliers:
- RANSAC:
  1. Randomly pick three points, and fit a plane to those points
  2. For every other point in the set, compute the distance between it and the plane:
	 dist = ax + by + cz + d / sqrt(a * a + b * b + c * c)
	 if dist > threshold, up a counter. 
  3. Repeat 1 and 2 for various random points, keeping track of all planes and distance counters
  4. Find the plane that had the highest counter, and recompute the correspondences. This is the dominant plane.
  5. (optional) throw out the points outside of the threshold. 
- Chebyshev's Inequality:
  A distribution function.
  p(|x - x0| >= k*sigma) <= 1/k^2
  Interpreted, this means that the chance of a point being within k * sigma of a mean is 1/K^2. 
- Normal distribution:
  Follows the normal 68 - 95- 99.7 rule for sigmas
- Hough Transform
  1. Divide your state function's variables into bins, e.g. for a line fitting, r = [+-N/sqrt(2)] for N image space, and theta = [-90, 90]
  2. For each point X in your dataset,
	 - Calculate the state function given one variable, and use that to fit the solved second variable to the closest bin
	   r = x * cos(theta) + y * sin(theta), then fit r to a bin
	 - Increment the count for that bin, i.e. add 1 to the count for line equation A(i,q) if theta-i results in r-q.
	 - Find all function candidates A(i,q) where A_count > threshold. 
- Robust M-estimation

Cholesky Decomposition:
- Purpose: Create a lower-triangular matrix that is easier to manipulate mathematically
- Process: ...

Conjugate Gradient Method: 

Iteratively Reweighted Least Squares Algorithm:
- 1. Find the weights of a problem through some metric (usually distance if looking for best line/plane fit)
  2. Solve for weighted average of all points, as well as the covariance in the solution. Remember to divide by the sum of weights!
  3. Solve for your state function variables
  4. Iterate until delta is small

## Duality ##

Why Duality:
- Three reasons:
  1. Sometimes the dual is eaier to solve than the primal
  2. Dual solution gives a lower bound to the primal solution
  3. Guaranteed convex

KKT Conditions and purpose:
- Purpose: necessary conditions for a first-order solution to be optimal (i.e. dual optimal -> primal optimal)
  1. Stationarity: min(-d(f)) = sum(d(inequal. dual)) + sum(d(equal. dual))
  2. Primal feasibility: g(x) <= 0, h(x) = 0 for all i and j
  3. Dual feasibility: lambda >= 0 for all i
  4. Complimentary slackness: lambda * g(x) = 0 for all i

Deriving the Dual:
- 1. if max, turn objective function into a min.
  2. Change all inequalities to make them <= 0
  3. Add dual variables to all conditions (lambda for inequality, nu for equality)
  4. Add these functions to the objective function
  5. Change the structure of the function from primal(dual func) to dual(primal func)
  6. Make all primal functions into conditions, and leave the dual function as the objective.
  7. If followed step 1, negate the objective formula again.

## Computer Vision Algorithms ##
Give purpose and process.

Lucas-Kanade:
- Purpose: Find the optical flow between images. Founded off of the brightness constancy constraint, which says that a pixel in one image will be about the same in the next image at the same coordinate.
- Process: Take the derivative of the current image in both X and Y, and map them per-pixel in a matrix A. Set this, multiplied by x, and set it to the change of the pixel intensity in time (aka the next iamge's pixel). Solving for this in a linear least-squares equation will give you the velocity of the flow in both x and y. 

Corner Detection:
- Purpose: ...detect a corner
- Process: Take the partial derivatives of the image in all directions. This will result in a matrix M. Solve for the eigenvalues of M. There are a couple of ways to find a corner here:
  1. if the minimal eigenvalue is under a certain value, then it's a corner (Tomasi-Kanade)
  2. If (e1* e2 * e3) - k(e1 + e2 + e3) > threshold, it's a corner (Harris)

ICP Algorithm:
- Purpose: Find the transformation that most closely maps two point clouds
- Process:

Phong Shading:
- Purpose: Realistic lighting conditions
- Process:
  I = Ka * ambient + Kd * (L * N) * diffuse + Ks * (R * V)^a * specular
  where
  ambient, diffuse, and specular are the light properties of a certain light
  Ka, Kd, and Ks are the coefficients of light
  R (reflection) = 2(L * N)N - L
  L is the light vector
  N is the normal vector
  V is the line of sight

## Kalman Filter ##

## Volume Raycasting ##
A way to 

## Bundle Adjustment ## 


