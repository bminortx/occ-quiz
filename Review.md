## Epipolar, Affine, Projective Geometry: ##

Epipolar Geometry, defined: 

Affine Geometry, defined:

Orthographic Projection: 

Camera Intrinsics:

Camera Extrinsics: 

Epipole:

Baseline:

Epipolar Plane:

Epipolar Line:

Infinity:
- Affine:
- Metric: 

Homography Matrix:
- Purpose:
- Compute from image correspondences:

Fundamental Matrix:
- Compute from homography:
- Compute from image correspondences: 

Projection from 2D to 3D:

Projection from 3D to 2D:

Depth of points in front of camera:

Depth from pair of stereo cameras:

## Image Transformations ##

Camera distortion:
- Given k1, k2, and r
- Barrel distortion
- Pincushion distortion

Taking the Partial Derivative of an Image:
- Forward Difference:
- Sobel Derivative:

Bilinear interpolation:
- Analytically:
- Linear Least Squares

Laplacian Pyramid: 

## Optimization Techniques ##

Newton Step:

Gauss-Newton:

Levenberg-Marquadt:

Solutions to A*x = 0:
- LU decomposition with pivoting:
- Kernel spaces:

Getting rid of outliers:
- RANSAC:
- Chebyshev's Inequality:
- Normal distribution:
- Hough Transform
- Robust M-estimation

Cholesky Decomposition:
- Purpose
- Process

Conjugate Gradient Method:

Iteratively Reweighted Least Squares Algorithm: 

## Duality ##

Why Duality: 

KKT Conditions and purpose:

Deriving the Dual:

## Computer Vision Algorithms ##
Give purpose and process.

Lucas-Kanade:
- Purpose
- Process

Corner Detection:
- Purpose
- Process

ICP Algorithm:
- Purpose
- Process

## Kalman Filter ##

## Volume Raycasting ##

## Bundle Adjustment ## 


