/*
  We want to track the position of a camera by processing images. However to
  take advantage of multiple CPU cores and keep the application responsive,
  we want to run the tracking code on a separate thread.

  TODO: implement the missing pseudo code in the Tracker class to handle and
  process new images. We want to process the images as fast as possible,
  and skip frames if the tracker cannot process them fast enough.
*/

#include <future>
#include <queue>

class Image;

class Tracker {
 public:
  Tracker ()
      : _imageToProcess (nullptr) {
    threadStart (this, &Tracker::run); // calls run on a background thread
  }

 public:
  void handleNewImage (Image* image) {
    image_queue_.push(image);
  }

 private:
  void run () {
    while (true) {
      if (!image_queue_.empty()) {
        // Timeout when necessary; believe this should work.
        std::future<bool> fut = std::async(processImage,
                                           image_queue_.front());
        std::chrono::milliseconds span(10);
        if (fut.wait_for(span) == std::future_status::timeout) {
          // Get rid of the image, move on.
          image_queue_.pop();
        }
      }
    }
  }
  void processImage (Image* image) { /* already implemented */ }

 private:
  std::queue<Image*> image_queue_;
};

int main () {
  Tracker tracker;
  while (true) {
    Image* image = getNextCameraImage ();
    if (image) {
      tracker.handleNewImage(image); // SHOULD NOT BLOCK THE MAIN THREAD
    }
    // process user input & UI refresh
  }
  return 0;
}
